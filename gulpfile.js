import gulp from "gulp";
import imagemin from "gulp-imagemin";
import autoprefixer from "gulp-autoprefixer";
import csso from "gulp-csso";
import clean from "gulp-clean";
import dartSass from "sass";
import gulpSass from "gulp-sass";

const { src, dest, watch, series, parallel } = gulp;

import bsc from "browser-sync";
const browserSync = bsc.create();
const sass = gulpSass(dartSass);

const htmlTaskHandler = () => {
    return src("./src/*.html").pipe(dest("./dist"));
};

const cssTaskHandler = () => {
    return src("./src/scss/main.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(autoprefixer())
        .pipe(csso())
        .pipe(dest("./dist/css"))
        .pipe(browserSync.stream());
};

const imagesTaskHandler = () => {
    return src("./src/images/**/*.*")
        .pipe(imagemin())
        .pipe(dest("./dist/images"));
};


const cleanDistTaskHandler = () => {
    return gulp.src('./dist', { allowEmpty: true })
        .pipe(clean());
  };

const browserSyncTaskHandler = () => {
    browserSync.init({
        server: {
            baseDir: "./dist",
        }
    });

    watch("./src/scss/**/*.scss").on(
        "all",
        series(cssTaskHandler, browserSync.reload)
    );
    watch("./src/*.html").on(
        "change",
        series(htmlTaskHandler, browserSync.reload)
    );
    watch("./src/img/**/*").on(
        "all",
        series(imagesTaskHandler, browserSync.reload)
    );
};

export const cleaning = cleanDistTaskHandler;
export const html = htmlTaskHandler;
export const css = cssTaskHandler;
export const images = imagesTaskHandler;

export const build = series(
    cleanDistTaskHandler,
    parallel(htmlTaskHandler, imagesTaskHandler, cssTaskHandler)
);
export const dev = series(build, browserSyncTaskHandler);


